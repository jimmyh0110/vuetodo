import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vClickOutside from 'v-click-outside'
import * as ModalDialogs from 'vue-modal-dialogs'


Vue.config.productionTip = false
Vue.use(vClickOutside)
Vue.use(ModalDialogs)

new Vue({
  router,
  ModalDialogs,
  render: h => h(App)
}).$mount('#app')
